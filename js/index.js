function createNewUser() {
    let firstName = prompt("Enter your first name");
    let lastName = prompt("Enter your last name");
    let [dd, mm, yyyy] = prompt("Enter your birthday").trim().split('.');
    let birthday = new Date(yyyy, Number(mm) - 1, dd);

    const newUser = {
        firstName,
        lastName,
        birthday,
        getLogin() {
            return this.firstName[0].toLowerCase().concat(this.lastName.toLowerCase());
        },
        getAge(){
            const ms = Date.now() - this.birthday.getTime();
            return Math.floor(ms / (1000 * 60 * 60 * 24 * 365.25));
        },
        getPassword(){
            return this.firstName[0].toUpperCase().concat(this.lastName.toLowerCase(), this.birthday.getFullYear());
        }


    };


    return newUser;
}


const newUser = createNewUser();

console.log(newUser.getLogin());

console.log(newUser.getAge());

console.log(newUser.getPassword());

